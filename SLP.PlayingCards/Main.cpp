//Playing Cards
//Stephanie Peebles

#include <iostream>
#include <conio.h>
#include <stdlib.h>

using namespace std;

enum Rank {
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit {
	HEARTS,
	DIAMONDS,
	CLUBS,
	SPADES
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard (Card card);
void HighCard(Card card1, Card card2);

int main()
{
	
	Card card1;
	Card card2;

	card1.rank = TWO;
	card1.suit = HEARTS;

	card2.rank = EIGHT;
	card2.suit = SPADES;


	PrintCard (card1);
	

	system("pause");

	HighCard(card1, card2);

	
	system("pause");
	return 0;
}

void PrintCard (Card card)
{
	cout << "The ";
	
		if (card.rank == 2)
			cout << "Two";
		else if (card.rank == 3)
			cout << "Three";
		else if (card.rank == 4)
			cout << "Four";
		else if (card.rank == 5)
			cout << "Five";
		else if (card.rank == 6)
			cout << "Six";
		else if (card.rank == 7)
			cout << "Seven";
		else if (card.rank == 8)
			cout << "Eight";
		else if (card.rank == 9)
			cout << "Nine";
		else if (card.rank == 10)
			cout << "Ten";
		else if (card.rank == 11)
			cout << "Jack";
		else if (card.rank == 12)
			cout << "Queen";
		else if (card.rank == 13)
			cout << "King";
		else
			cout << "Ace";
	
	cout << " of ";
		
		if (card.suit == HEARTS)
			cout << "Hearts";
		else if (card.suit == DIAMONDS)
			cout << "Diamonds";
		else if (card.suit == CLUBS)
			cout << "Clubs";
		else 
			cout << "Spades";

}

void HighCard(Card card1, Card card2)
{
	if(card1.rank < card2.rank){
		PrintCard(card2); 

	}
	else
	{
		PrintCard(card1); 
	}
	
	cout << " is the highest card.\n";


}